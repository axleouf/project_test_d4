package com.decathlon.nicolasbrosse.component;

import com.decathlon.nicolasbrosse.data.manager.GitHubManager;
import com.decathlon.nicolasbrosse.data.manager.OauthManager;
import com.decathlon.nicolasbrosse.data.manager.RepositoryDao;
import com.decathlon.nicolasbrosse.data.manager.RepositoryManager;
import com.decathlon.nicolasbrosse.ui.MainActivity;
import com.decathlon.nicolasbrosse.component.module.ContextModule;
import com.decathlon.nicolasbrosse.component.module.GitHubModule;
import com.decathlon.nicolasbrosse.ui.login.LoginFragment;
import com.decathlon.nicolasbrosse.ui.repos.ListRepoTabFragment;
import com.decathlon.nicolasbrosse.ui.repos.ReposFragment;
import com.decathlon.nicolasbrosse.ui.repos.detail.DetailRepoFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(
        modules = {ContextModule.class, GitHubModule.class})
public interface GitHubComponent {

    GitHubManager gitHubManager();
    OauthManager oauthManager();
    RepositoryManager RepositoryManager();

    RepositoryDao repositoryDao();

    void inject(MainActivity mainActivity);
    void inject(LoginFragment loginFragment);
    void inject(ReposFragment reposFragment);
    void inject(ListRepoTabFragment listRepoTabFragment);
    void inject(DetailRepoFragment detailRepoFragment);
}