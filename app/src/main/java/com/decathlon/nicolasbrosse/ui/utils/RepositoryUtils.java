package com.decathlon.nicolasbrosse.ui.utils;

import com.decathlon.nicolasbrosse.data.model.Owner;
import com.decathlon.nicolasbrosse.data.model.Permission;
import com.decathlon.nicolasbrosse.data.model.Repository;
import com.decathlon.nicolasbrosse.data.model.dto.OwnerDto;
import com.decathlon.nicolasbrosse.data.model.dto.PermissionDto;
import com.decathlon.nicolasbrosse.data.model.dto.RepositoryDto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by BROSSE Nicolas on 13/04/2017.
 */

public class RepositoryUtils {

    public static boolean isCanEdit(Repository repository, Owner owner){

        if(null != repository && null != repository.getPermissions() && repository.getPermissions().isAdmin()){
            return true;
        }

        return null != repository && null != repository.getOwner() && owner != null && repository.getOwner().getId().equalsIgnoreCase(owner.getId());

    }

    public static List<RepositoryDto> convertListRepotoListRepoDto(List<Repository> repositories) {
        List<RepositoryDto> repositoryDtos = new ArrayList<>();
        for (Repository repository : repositories) {
            repositoryDtos.add(convertRepositoryToRepositoryDto(repository));
        }
        return repositoryDtos;
    }

    public static List<Repository> convertListRepoDtotoListRepo(List<RepositoryDto> repositoriesDtos) {
        List<Repository> repositories = new ArrayList<>();
        for (RepositoryDto repositoryDto : repositoriesDtos) {
            repositories.add(convertRepositoryDtoToRepository(repositoryDto));
        }
        return repositories;
    }

    public static RepositoryDto convertRepositoryToRepositoryDto(Repository repository) {

        PermissionDto permissionDto = new PermissionDto();
        if (null != repository.getPermissions()) {
            permissionDto.setAdmin(repository.getPermissions().isAdmin());
        }

        OwnerDto ownerDto = new OwnerDto();
        ownerDto.setId(repository.getOwner().getId());
        ownerDto.setLogin(repository.getOwner().getLogin());
        ownerDto.setAvatarUrl(repository.getOwner().getAvatarUrl());

        RepositoryDto repositoryDto = new RepositoryDto();
        repositoryDto.setId(repository.getId());
        repositoryDto.setDescription(repository.getDescription());
        repositoryDto.setFullName(repository.getFullName());
        repositoryDto.setWatchersCount(repository.getWatchersCount());
        repositoryDto.setHomepage(repository.getHomepage());
        repositoryDto.setName(repository.getName());
        repositoryDto.setOwner(ownerDto);
        repositoryDto.setPermissions(permissionDto);
        return repositoryDto;
    }

    public static Repository convertRepositoryDtoToRepository(RepositoryDto repositoryDto) {

        Permission permission = new Permission();
        if (null != repositoryDto.getPermissions()) {
            permission.setAdmin(repositoryDto.getPermissions().isAdmin());
        }

        Owner owner = new Owner();
        owner.setId(repositoryDto.getOwner().getId());
        owner.setLogin(repositoryDto.getOwner().getLogin());
        owner.setAvatarUrl(repositoryDto.getOwner().getAvatarUrl());

        Repository repository = new Repository();
        repository.setId(repositoryDto.getId());
        repository.setDescription(repositoryDto.getDescription());
        repository.setFullName(repositoryDto.getFullName());
        repository.setWatchersCount(repositoryDto.getWatchersCount());
        repository.setHomepage(repositoryDto.getHomepage());
        repository.setName(repositoryDto.getName());
        repository.setOwner(owner);
        repository.setPermissions(permission);
        return repository;
    }

}
