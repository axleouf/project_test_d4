package com.decathlon.nicolasbrosse.ui.repos.detail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.decathlon.nicolasbrosse.GitHubApplication;
import com.decathlon.nicolasbrosse.R;
import com.decathlon.nicolasbrosse.data.manager.GitHubManager;
import com.decathlon.nicolasbrosse.data.manager.OauthManager;
import com.decathlon.nicolasbrosse.data.manager.RepositoryManager;
import com.decathlon.nicolasbrosse.data.model.Owner;
import com.decathlon.nicolasbrosse.data.model.Repository;
import com.decathlon.nicolasbrosse.data.model.event.UpdateListRepo;
import com.decathlon.nicolasbrosse.ui.base.BaseFragment;
import com.decathlon.nicolasbrosse.ui.utils.ViewUtils;


import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiConsumer;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * Created by BROSSE Nicolas on 12/04/2017.
 */

public class DetailRepoFragment extends BaseFragment {

    private static String TAG = DetailRepoFragment.class.getSimpleName();

    @Inject
    GitHubManager gitHubManager;

    @Inject
    OauthManager oauthManager;

    @Inject
    RepositoryManager repositoryManager;

    @BindView(R.id.create_repo_et_name)
    TextInputEditText mEtName;

    @BindView(R.id.create_repo_et_description)
    TextInputEditText mEtDescription;

    @BindView(R.id.create_repo_tv_name)
    TextView mTvName;

    @BindView(R.id.create_repo_tv_description)
    TextView mTvDescription;

    @BindView(R.id.create_repo_til_name)
    TextInputLayout mTilName;

    @BindView(R.id.create_repo_til_description)
    TextInputLayout mTilDescription;

    @BindView(R.id.create_repo_bt_create_or_update)
    Button mBtCreateOrUpdate;

    private CompositeDisposable compositeDisposable;

    private Repository mRepository;

    @Override
    public String getTitle() {
        Bundle arguments = getArguments();
        if (null == arguments) {
            return getString(R.string.create_new_repo_title);
        }
        return "";
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_create_repo;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        GitHubApplication.getAppGitHubComponent(getActivity()).inject(this);

        compositeDisposable = new CompositeDisposable();

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);

        Bundle arguments = getArguments();
        if (null == arguments) {
            toolbar.setTitle(getString(R.string.create_new_repo_title));
            return;
        }

        boolean showDetailRepo = arguments.getBoolean("showDetailRepo", false);
        boolean isCanEdit = arguments.getBoolean("canEdit", false);

        setVisibilityField(showDetailRepo);

        if (showDetailRepo) {

            mRepository = (Repository) arguments.getSerializable("repository");
            if(null == mRepository){
                return;
            }

            String name = mRepository.getName();
            String description = mRepository.getDescription();

            if(!TextUtils.isEmpty(name)){
                toolbar.setTitle(name);
                mEtName.setText(name);
                mTvName.setText(name);
            }

            if(!TextUtils.isEmpty(description)){
                mEtDescription.setText(description);
                mTvDescription.setText(description);
            }

            mBtCreateOrUpdate.setVisibility(View.GONE);
        }

        if(isCanEdit){
            setHasOptionsMenu(true);
            toolbar.inflateMenu(R.menu.menu_detail_repo);
        }
    }

    private void setVisibilityField(boolean showDetailRepo) {
        mTilName.setVisibility(showDetailRepo ? View.INVISIBLE : View.VISIBLE);
        mTilDescription.setVisibility(showDetailRepo ? View.INVISIBLE : View.VISIBLE);

        mTvName.setVisibility(showDetailRepo ? View.VISIBLE : View.INVISIBLE);
        mTvDescription.setVisibility(showDetailRepo ? View.VISIBLE : View.INVISIBLE);

        if (showDetailRepo) {
            mBtCreateOrUpdate.setText(getString(R.string.create_repo_create));
        } else {
            mBtCreateOrUpdate.setText(getString(R.string.create_repo_update));
            mBtCreateOrUpdate.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_detail_repo, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_detail_repo_edit: {
                setVisibilityField(false);
            }
            return true;
            case R.id.menu_detail_repo_delete: {
                deleteRepository();
            }
            return true;
            default:
                break;
        }

        return false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (null != compositeDisposable) {
            compositeDisposable.clear();
            compositeDisposable.dispose();
        }
    }

    @OnClick(R.id.create_repo_bt_create_or_update)
    public void onClickBtCreateOrUpdate() {
        if (mBtCreateOrUpdate.getText().toString().equalsIgnoreCase(getString(R.string.create_repo_create))) {
            createRepository();
        } else {
            updateRepository();
        }
    }

    private void createRepository() {
        Repository repository = getRepositoryInput();

        if (repository == null) {
            return;
        }

        Disposable disposable = gitHubManager.createRepository(repository)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .singleOrError()
                .subscribe(new BiConsumer<Repository, Throwable>() {
                    @Override
                    public void accept(@NonNull Repository repository, @NonNull Throwable throwable) throws Exception {
                        if(null != repository){
                            repositoryManager.createOrUpdate(repository);
                            EventBus.getDefault().post(new UpdateListRepo(true));
                            ViewUtils.hideKeyboard(getActivity());
                            getFragmentManager().popBackStack();
                        }

                        if(null != throwable){
                            Toast.makeText(getActivity(), R.string.create_repo_error_server, Toast.LENGTH_SHORT).show();
                        }
                    }
                });

        compositeDisposable.add(disposable);
    }

    private void updateRepository() {
        Repository repository = getRepositoryInput();
        if (null == repository) {
            return;
        }

        Owner owner = oauthManager.getOwner();
        if(null == owner || null == mRepository){
            return;
        }

        Disposable disposable = gitHubManager.updateRepository(owner.getLogin(), mRepository.getName(), repository)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .singleOrError()
                .subscribe(new Consumer<Repository>() {
                    @Override
                    public void accept(@NonNull Repository repository) throws Exception {
                        repositoryManager.createOrUpdate(repository);
                        EventBus.getDefault().post(new UpdateListRepo(true));
                        ViewUtils.hideKeyboard(getActivity());
                        getFragmentManager().popBackStack();
                    }
                });

        compositeDisposable.add(disposable);
    }

    private void deleteRepository() {

        Owner owner = oauthManager.getOwner();
        if(null == owner || null == mRepository){
            return;
        }

        Disposable disposable = gitHubManager.deleteRepository(owner.getLogin(), mRepository.getName())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .singleOrError()
                .subscribe(new Consumer<Response<Void>>() {
                    @Override
                    public void accept(@NonNull Response<Void> voidResponse) throws Exception {
                        repositoryManager.delete(mRepository);
                        EventBus.getDefault().post(new UpdateListRepo(false));
                        getFragmentManager().popBackStack();
                    }
                });

        compositeDisposable.add(disposable);
    }

    @Nullable
    private Repository getRepositoryInput() {
        String name = mEtName.getText().toString();
        if (TextUtils.isEmpty(name)) {
            mEtName.setError(getString(R.string.create_repo_field_required));
            return null;
        }

        String description = mEtDescription.getText().toString();

        if (TextUtils.isEmpty(description)) {
            mEtDescription.setError(getString(R.string.create_repo_field_required));
            return null;
        }

        Repository repository = new Repository();
        repository.setName(name);
        repository.setDescription(description);
        repository.setHomepage("https://github.com");
        return repository;
    }


}