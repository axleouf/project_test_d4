package com.decathlon.nicolasbrosse.ui.repos;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.decathlon.nicolasbrosse.GitHubApplication;
import com.decathlon.nicolasbrosse.R;
import com.decathlon.nicolasbrosse.data.manager.GitHubManager;
import com.decathlon.nicolasbrosse.data.manager.OauthManager;
import com.decathlon.nicolasbrosse.data.manager.RepositoryManager;
import com.decathlon.nicolasbrosse.data.model.Repository;
import com.decathlon.nicolasbrosse.data.model.Search;
import com.decathlon.nicolasbrosse.data.model.event.UpdateListRepo;
import com.decathlon.nicolasbrosse.ui.repos.detail.DetailRepoFragment;
import com.decathlon.nicolasbrosse.ui.repos.holder.RecyclerViewAdapter;
import com.decathlon.nicolasbrosse.ui.utils.RepositoryUtils;
import com.decathlon.nicolasbrosse.ui.utils.ViewUtils;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.jakewharton.rxbinding2.widget.TextViewAfterTextChangeEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiConsumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by BROSSE Nicolas on 12/04/2017.
 */

public class ListRepoTabFragment extends Fragment {

    @Inject
    GitHubManager gitHubManager;

    @Inject
    OauthManager oauthManager;

    @Inject
    RepositoryManager repositoryManager;

    @BindView(R.id.list_repo_tab_recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.list_repo_tab_bt_create_repo)
    Button mBtCreateRepo;

    @BindView(R.id.list_repo_tab_et_search)
    EditText mEtSearch;

    Unbinder unbinder;

    private RecyclerViewAdapter mRecyclerViewAdapter;

    private CompositeDisposable compositeDisposable;

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_repo_tab, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        GitHubApplication.getAppGitHubComponent(getActivity()).inject(this);

        compositeDisposable = new CompositeDisposable();

        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        mRecyclerViewAdapter = new RecyclerViewAdapter();
        mRecyclerViewAdapter.setOnClickListenerCustom(new RecyclerViewAdapter.OnClickListenerCustom() {
            @Override
            public void onClick(int position) {
                goDetailRepoFragment(position);
            }
        });

        mRecyclerView.setAdapter(mRecyclerViewAdapter);

        boolean isScreenListRepos = getArguments().getBoolean("screenListRepos");
        mBtCreateRepo.setVisibility(isScreenListRepos ? View.VISIBLE : View.INVISIBLE);
        refreshData(repositoryManager.getRepositories());

        RxTextView.afterTextChangeEvents(mEtSearch)
                .debounce(500, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<TextViewAfterTextChangeEvent>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(TextViewAfterTextChangeEvent textViewAfterTextChangeEvent) {
                        if (null != textViewAfterTextChangeEvent && !TextUtils.isEmpty(textViewAfterTextChangeEvent.view().getText().toString())) {
                            String query = textViewAfterTextChangeEvent.view().getText().toString();
                            if (query.length() > 3) {
                                downloadDataSearch(query);
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        if (null != compositeDisposable) {
            compositeDisposable.clear();
            compositeDisposable.dispose();
        }
    }

    @OnClick(R.id.list_repo_tab_bt_create_repo)
    public void onClickBtCreateRepo() {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.main_container, new DetailRepoFragment(), "detail");
        fragmentTransaction.addToBackStack("github");
        fragmentTransaction.commit();
    }

    @Subscribe
    public void onUpdateListRepoEvent(UpdateListRepo event) {
        refreshData(repositoryManager.getRepositories());
        if (event.isCreateOrUpdate()) {
            mRecyclerView.smoothScrollToPosition(mRecyclerViewAdapter.getItemCount());
        }
    }

    private void downloadDataSearch(String query) {
        if (!TextUtils.isEmpty(query)) {
            Observable<Search> searchObservable = gitHubManager.searchRepositories(query);
            subscriberSearch(searchObservable);
        }
    }

    private void refreshData(List<Repository> repositories) {
        if (null != repositories && repositories.size() > 0) {
            mRecyclerViewAdapter.addAll(repositories);
        }
    }

    private void subscriberSearch(Observable<Search> searchObservable) {
        Disposable disposable = searchObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .singleOrError()
                .subscribe(new BiConsumer<Search, Throwable>() {
                    @Override
                    public void accept(@NonNull Search search, @NonNull Throwable throwable) throws Exception {
                        if (null != search && null != search.getRepositories()) {
                            refreshData(search.getRepositories());
                        }
                    }
                });
        compositeDisposable.add(disposable);
    }

    private void goDetailRepoFragment(int position) {
        Repository repository = mRecyclerViewAdapter.getItem(position);

        Bundle extras = new Bundle();

        extras.putBoolean("showDetailRepo", true);
        extras.putBoolean("canEdit", RepositoryUtils.isCanEdit(repository, oauthManager.getOwner()));
        extras.putSerializable("repository", repository);

        DetailRepoFragment fragment = new DetailRepoFragment();
        fragment.setArguments(extras);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack("github");
        fragmentTransaction.add(R.id.main_container, fragment);
        fragmentTransaction.commit();
    }
}
