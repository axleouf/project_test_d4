package com.decathlon.nicolasbrosse;

import android.app.Application;
import android.content.Context;
import com.decathlon.nicolasbrosse.component.DaggerGitHubComponent;
import com.decathlon.nicolasbrosse.component.GitHubComponent;
import com.decathlon.nicolasbrosse.component.module.ContextModule;

/**
 * Created by BROSSE Nicolas on 12/04/2017.
 */

public class GitHubApplication extends Application {

    protected GitHubComponent gitHubComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        gitHubComponent = DaggerGitHubComponent.builder()
                .contextModule(new ContextModule(getApplicationContext()))
                .build();
    }

    public GitHubComponent component() {
        return gitHubComponent;
    }

    public static GitHubComponent getAppGitHubComponent(Context context) {
        GitHubApplication application = (GitHubApplication) context.getApplicationContext();
        return application.gitHubComponent;
    }
}