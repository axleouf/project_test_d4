package com.decathlon.nicolasbrosse.data.manager;

import com.decathlon.nicolasbrosse.data.model.Repository;

import java.util.List;

/**
 * Created by BROSSE Nicolas on 13/04/2017.
 */

public interface RepositoryManager {

    void saveRepositories(List<Repository> repositories);

    List<Repository> getRepositories();

    Repository getRepository(int id);

    void createOrUpdate(Repository repository);

    void delete(Repository repository);

}
