package com.decathlon.nicolasbrosse.data.model.event;

/**
 * Created by BROSSE Nicolas on 13/04/2017.
 */

public class UpdateListRepo {

    private boolean createOrUpdate;

    public UpdateListRepo(boolean createOrUpdate) {
        this.createOrUpdate = createOrUpdate;
    }

    public boolean isCreateOrUpdate() {
        return createOrUpdate;
    }

    public void setCreateOrUpdate(boolean createOrUpdate) {
        this.createOrUpdate = createOrUpdate;
    }
}
