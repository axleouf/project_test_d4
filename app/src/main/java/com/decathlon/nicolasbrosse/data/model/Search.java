package com.decathlon.nicolasbrosse.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by BROSSE Nicolas on 12/04/2017.
 */

public class Search implements Parcelable{

    @SerializedName("total_count")
    private long totalCount;

    @SerializedName("items")
    private List<Repository> repositories;

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    public List<Repository> getRepositories() {
        return repositories;
    }

    public void setRepositories(List<Repository> repositories) {
        this.repositories = repositories;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.totalCount);
        dest.writeTypedList(this.repositories);
    }

    public Search() {
    }

    protected Search(Parcel in) {
        this.totalCount = in.readLong();
        this.repositories = in.createTypedArrayList(Repository.CREATOR);
    }

    public static final Creator<Search> CREATOR = new Creator<Search>() {
        @Override
        public Search createFromParcel(Parcel source) {
            return new Search(source);
        }

        @Override
        public Search[] newArray(int size) {
            return new Search[size];
        }
    };
}
