package com.decathlon.nicolasbrosse.data.persistence;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;

import com.google.gson.Gson;

import java.lang.reflect.Type;

public class PreferencesRepository {

    private final Context mApplication;
    private final String mPreferencesName;
    private final Gson mGson;

    protected PreferencesRepository(Context context, String preferenceName, Gson gson) {
        mApplication = context;
        mPreferencesName = preferenceName;
        mGson = gson;
    }

    private SharedPreferences getPreferences() {
        return mApplication.getSharedPreferences(mPreferencesName, Context.MODE_PRIVATE);
    }

    protected <T> void saveObject(String key, T obj) {
        if (obj == null) {
            return;
        }

        String json = mGson.toJson(obj);
        getPreferences().edit()
                .putString(key, json)
                .apply();
    }

    protected void delete(String key) {
        getPreferences().edit()
                .remove(key)
                .apply();
    }

    @Nullable
    protected <T> T findObject(String key, Class<T> clz) {
        String json = getPreferences().getString(key, null);
        if (json == null) {
            return null;
        }

        return mGson.fromJson(json, clz);
    }

    @Nullable
    protected <T> T findObject(String key, Type type) {
        String json = getPreferences().getString(key, null);
        if (json == null) {
            return null;
        }

        return mGson.fromJson(json, type);
    }
}