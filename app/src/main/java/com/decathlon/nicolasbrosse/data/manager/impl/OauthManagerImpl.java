package com.decathlon.nicolasbrosse.data.manager.impl;

import android.text.TextUtils;
import android.util.Base64;

import com.decathlon.nicolasbrosse.data.manager.OauthManager;
import com.decathlon.nicolasbrosse.data.model.LoginAccess;
import com.decathlon.nicolasbrosse.data.model.Owner;
import com.decathlon.nicolasbrosse.data.persistence.OwnerRepository;

/**
 * Created by BROSSE Nicolas on 12/04/2017.
 */

public class OauthManagerImpl implements OauthManager {

    private String mUsernamePasswordBase64;

    private OwnerRepository mOwnerRepository;

    private Owner mOwner;

    public OauthManagerImpl(OwnerRepository ownerRepository){
        this.mOwnerRepository = ownerRepository;
    }

    @Override
    public String getAccessToken() {
        return "d5e9cc2ab84e696808c8eaa290951b96dae92f25";
    }

    @Override
    public void setUsernamePasswordBase64(String username, String password) {
        try {
            byte[] data = username.concat(":").concat(password).getBytes("UTF-8");
            mUsernamePasswordBase64 =  Base64.encodeToString(data, Base64.DEFAULT);
        }catch (Exception e){
            //do nothing
            mUsernamePasswordBase64 = null;
        }
    }

    @Override
    public String getUsernamePasswordBase64() {
        LoginAccess loginAccess = mOwnerRepository.findLoginAccess();
        if(null != loginAccess){
            try {
                String username = loginAccess.getUsername();
                String password = loginAccess.getPassword();

                if(!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password)){
                    byte[] data = username.concat(":").concat(password).getBytes("UTF-8");
                    return Base64.encodeToString(data, Base64.DEFAULT);
                }
            }catch (Exception e){
                //do nothing
            }
        }

        return mUsernamePasswordBase64;
    }

    @Override
    public void saveAccess(LoginAccess loginAccess) {
        mOwnerRepository.saveAccess(loginAccess);
    }

    @Override
    public LoginAccess getAccess() {
        return mOwnerRepository.findLoginAccess();
    }

    @Override
    public void saveOwner(Owner owner) {
        mOwner = owner;
        mOwnerRepository.saveOwner(owner);
    }

    public Owner getOwner() {
        if(null == mOwner){
            mOwner = mOwnerRepository.findOwner();
            return mOwner;
        }else {
            return mOwner;
        }
    }

    @Override
    public boolean isConnected() {
        return mOwnerRepository.findOwner() != null;
    }
}
