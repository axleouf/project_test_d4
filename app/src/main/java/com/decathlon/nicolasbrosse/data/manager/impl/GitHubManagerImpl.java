package com.decathlon.nicolasbrosse.data.manager.impl;

import android.text.TextUtils;

import com.decathlon.nicolasbrosse.data.manager.GitHubManager;
import com.decathlon.nicolasbrosse.data.manager.OauthManager;
import com.decathlon.nicolasbrosse.data.model.LoginAccess;
import com.decathlon.nicolasbrosse.data.model.Owner;
import com.decathlon.nicolasbrosse.data.model.Repository;
import com.decathlon.nicolasbrosse.data.model.Search;
import com.decathlon.nicolasbrosse.data.service.GitHubService;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Action;
import retrofit2.Response;

/**
 * Created by BROSSE Nicolas on 12/04/2017.
 */

public class GitHubManagerImpl implements GitHubManager {

    private GitHubService mGitHubService;
    private OauthManager mOauthManager;

    public GitHubManagerImpl(GitHubService gitHubService, OauthManager oauthManager) {
        this.mGitHubService = gitHubService;
        this.mOauthManager = oauthManager;
    }

    @Override
    public Observable<Owner> login(final String username, final String password) {
        try {
            mOauthManager.setUsernamePasswordBase64(username, password);
            return mGitHubService.getUser().doOnTerminate(new Action() {
                @Override
                public void run() throws Exception {
                    mOauthManager.saveAccess(new LoginAccess(username, password));
                }
            });
        } catch (Exception e) {
            return Observable.error(e);
        }
    }

    @Override
    public Observable<Owner> autoLogin() {
        if(mOauthManager.isConnected()){
            String username = mOauthManager.getAccess().getUsername();
            String password = mOauthManager.getAccess().getPassword();

            if(!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password)){
                return login(username, password);
            }
        }

        return Observable.error(new Exception("there is problem in autologin"));
    }

    @Override
    public Observable<List<Repository>> getRepositories() {
        return mGitHubService.getRepositories();
    }

    @Override
    public Observable<Search> searchRepositories(String q) {
        return mGitHubService.searchRepositories(q);
    }

    @Override
    public Observable<Repository> createRepository(Repository repository) {
        return mGitHubService.createRepository(repository);
    }

    @Override
    public Observable<Repository> updateRepository(String owner, String repositoryName, Repository repository) {
        return mGitHubService.updateRepository(owner, repositoryName, repository);
    }

    @Override
    public Observable<Response<Void>> deleteRepository(String owner, String repositoryName) {
        return mGitHubService.deleteRepository(owner, repositoryName);
    }
}