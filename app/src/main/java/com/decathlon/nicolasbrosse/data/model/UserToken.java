package com.decathlon.nicolasbrosse.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by BROSSE Nicolas on 12/04/2017.
 */

public class UserToken implements Parcelable{

    private String access_token;

    private String scope;

    private String refresh_token;

    private String token_type;

    private long expires_in;

    private long local_expire_time;

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public long getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(long expires_in) {
        this.expires_in = expires_in;
    }

    public long getLocal_expire_time() {
        return local_expire_time;
    }

    public void setLocal_expire_time(long local_expire_time) {
        this.local_expire_time = local_expire_time;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.access_token);
        dest.writeString(this.scope);
        dest.writeString(this.refresh_token);
        dest.writeString(this.token_type);
        dest.writeLong(this.expires_in);
        dest.writeLong(this.local_expire_time);
    }

    public UserToken() {
    }

    protected UserToken(Parcel in) {
        this.access_token = in.readString();
        this.scope = in.readString();
        this.refresh_token = in.readString();
        this.token_type = in.readString();
        this.expires_in = in.readLong();
        this.local_expire_time = in.readLong();
    }

    public static final Creator<UserToken> CREATOR = new Creator<UserToken>() {
        @Override
        public UserToken createFromParcel(Parcel source) {
            return new UserToken(source);
        }

        @Override
        public UserToken[] newArray(int size) {
            return new UserToken[size];
        }
    };
}
