package com.decathlon.nicolasbrosse.data.model.dto;

import io.realm.RealmObject;

/**
 * Created by BROSSE Nicolas on 13/04/2017.
 */

public class PermissionDto extends RealmObject {

    private boolean admin;

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }
}
