package com.decathlon.nicolasbrosse.data.manager;

import com.decathlon.nicolasbrosse.data.model.Owner;
import com.decathlon.nicolasbrosse.data.model.Repository;
import com.decathlon.nicolasbrosse.data.model.Search;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by BROSSE Nicolas on 12/04/2017.
 */

public interface GitHubManager {

    Observable<Owner> login(String username, String password);

    Observable<Owner> autoLogin();

    Observable<List<Repository>> getRepositories();

    Observable<Search> searchRepositories(String q);

    Observable<Repository> createRepository(Repository repository);

    Observable<Repository> updateRepository(String owner, String repositoryName, Repository repository);

    Observable<Response<Void>> deleteRepository(String owner, String repositoryName);
}
