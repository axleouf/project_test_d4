package com.decathlon.nicolasbrosse.data.model;

import java.util.List;

/**
 * Created by BROSSE Nicolas on 13/04/2017.
 */

public class ModelZipLoginAndRepositories {

    private Owner owner;

    private List<Repository> repositories;

    public ModelZipLoginAndRepositories(Owner owner, List<Repository> repositories) {
        this.owner = owner;
        this.repositories = repositories;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public List<Repository> getRepositories() {
        return repositories;
    }

    public void setRepositories(List<Repository> repositories) {
        this.repositories = repositories;
    }
}
